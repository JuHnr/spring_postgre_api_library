package co.simplon.Rest.Library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.Rest.Library.entity.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    
    // Note : Les méthodes CRUD sont contenues dans JpaRepository dont hérite l'interface

    // Query method pour rechercher un livre par son titre
    Book findByTitle(String title);
}
